# RandLA-Net

#### 介绍
Tensorflow 论文复现 RandLA-Net: Efficient Semantic Segmentation of Large-Scale Point Clouds (CVPR 2020)

#### 软件架构
本次测试是在tensorflow1.15, ModelArts云服务器下进行，无需环境搭建

在Ubuntu18.04上运行，需搭建环境，命令如下：

```
conda create -n tensorflow tensorflow==1.15
conda activate tensorflow
conda install -n tensorflow tensorflow-gpu==1.15
pip install open3d-python #相关依赖包
sh compile_op.sh
```



#### 安装教程

1.  下载代码

```
git clone --depth=1 https://gitee.com/xuanzhang_1/RandLa-Net.git
```

2.  配置ModelArts Training Job Configuration相关参数
![输入图片说明](https://images.gitee.com/uploads/images/2021/0906/221829_2d05e202_9688936.png "屏幕截图.png")


#### 使用说明

1.  S3DIS数据集[下载地址](http://https://docs.google.com/forms/d/e/1FAIpQLScDimvNMCGhy_rmBA2gHfDu3naktRm6A8BPwAWWDv-Uhm6Shw/viewform?c=0&w=1)，下载名为"Stanford3dDataset_v1.2_Aligned_Version.zip"的文件并解压，并及时更新数据预处理程序utils/data_prepare_s3dis.py内文件地址，之后执行下面预处理的命令。预处理之后的数据下载链接：https://pan.baidu.com/s/1vy7VPMzqXrijYp9lz96vFw 提取码：wo5m。下载好数据之后，上传至OBS桶。

```
python utils/data_prepare_s3dis.py
``` 

2.  训练命令、验证命令、测试命令执行下面.py文件

```
python run_sh_S3DIS.py
```

3.  标杆性能比对结果

4.  标杆loss曲线比对结果
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
